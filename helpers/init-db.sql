CREATE TABLE IF NOT EXISTS robot_club_person_status (
	person_id INTEGER PRIMARY KEY,
	robot_name VARCHAR(255) NOT NULL,
	robot_level INTEGER NOT NULL DEFAULT 0,
	-- 0 = Human
	-- 1 = Cyborg
	-- 2 = Gynoid
	-- 3 = Sexbot
	
	update_day INTEGER DEFAULT -1, -- Next day on which actions done by the Principal on the cub's mainframe will be applied
	
	gender_at_conversion VARCHAR(64) NOT NULL DEFAULT 'Female',
	default_gender VARCHAR(64) NOT NULL DEFAULT 'Female',
	
	genitals_deployed BOOLEAN NOT NULL DEFAULT FALSE,
	
	-- Whether a parameter is forced by a controller
	-- Set to -1 or empty string to disable forcing
	-- CL = control level of the controller issuing the override
	-- Needs higher control level to over-override
	force_gender VARCHAR(64) NOT NULL DEFAULT '',
	force_gender_cl INTEGER NOT NULL DEFAULT -1,
	force_outfit_level INTEGER NOT NULL DEFAULT -1,
	force_outfit_cl INTEGER NOT NULL DEFAULT -1,
	force_location VARCHAR(255) NOT NULL DEFAULT '',
	force_location_cl INTEGER NOT NULL DEFAULT -1,
	force_deploy_genitals INTEGER NOT NULL DEFAULT -1,
	force_genitals_cl INTEGER DEFAULT -1
)

CREATE TABLE IF NOT EXISTS robot_club_person_control (
	person_id INTEGER NOT NULL,
	controller_id INTEGER NOT NULL,
	
	controller_designation VARCHAR(255) NOT NULL DEFAULT 'Controller',
	control_level INTEGER NOT NULL DEFAULT 0,
	
	/*
	 * Control level:
	 * 0 = No actual control, only designation
	 * 1 = Summon to location, get personal data, read stats
	 * 2 = Change gender, change body sizes, deploy/stow genitals
	 * 3 = Force location, perform sexual action
	 * 4 = Change relationship, change home
	 * 5 = Force genitals, force gender, force outfit level, trigger trance
	 *
	 */
	
	PRIMARY KEY (person_id, controller_id)
)