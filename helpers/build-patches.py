
import subprocess
import os
import os.path
import sys

BASENAME = 'RoboticsClub'
BASEDIR = 'HHS+ModsUpload'

SMALLVILLE_FOLDER = "NormalSchool"
LEXVILLE_FOLDER = "ModSchool"
LEXVILLE_OVERWRITE_FOLDER = "ModSchool.overwrites"

OLDEST_PATCH = 7

def makezip(zipname, *files):
	print(">> Build {} using {!r}".format(zipname, files))
	subprocess.run(['zip', '-r', '-q', '-9', '-T', '-n', '.png:.jpg:.jpeg:.gif', zipname] + list(files), check = True, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)

def mklexville():
	print(">> Create Lexville version")
	os.mkdir(LEXVILLE_FOLDER)
	subprocess.run('cp -Rf {}/* {}'.format(SMALLVILLE_FOLDER, LEXVILLE_FOLDER), shell = True, check = True, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)
	subprocess.run(['python3', 'helpers/convert-to-lexville.py', SMALLVILLE_FOLDER, LEXVILLE_FOLDER], check = True, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)
	subprocess.run('cp -Rf {}/* {}'.format(LEXVILLE_OVERWRITE_FOLDER, LEXVILLE_FOLDER), shell = True, check = True, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)

def changed_files(version, backwards = 1, lexville = False):
	old = 'rel-' + str(version-backwards)
	new = 'rel-' + str(version)
	print(">> List changed files between refs {!r} and {!r} for {}".format(old, new, 'Lexville' if lexville else 'Smallville'))
	run = subprocess.run(['git', 'diff', '--name-only', old, new], check=True, stdout=subprocess.PIPE)
	if lexville:
		return {os.path.join(LEXVILLE_FOLDER, path.split(os.path.sep, 1)[1]) for path in run.stdout.decode('utf8').split('\n') if path.split(os.path.sep, 1)[0] in (SMALLVILLE_FOLDER, LEXVILLE_OVERWRITE_FOLDER)}
	else:
		return {path for path in run.stdout.decode('utf8').split('\n') if path.split(os.path.sep, 1)[0] == SMALLVILLE_FOLDER}

def build_patch(old, new, lexville = False):
	print(">  Build patch from ver {} to ver {} for {}".format(old, new, 'Lexville' if lexville else 'Smallville'))
	back = new - old
	zipname = BASENAME + ('-lexville' if lexville else '-smallville') + '-up_' + str(old) + '_to_' + str(new) + '.zip'
	include = changed_files(new, back, lexville)
	makezip(zipname, 'README.md', 'LICENSE', *include)
	return zipname

def build_full(ver, lexville = False):
	print(">  Build full version {} for {}".format(ver, 'Lexville' if lexville else 'Smallville'))
	zipname = BASENAME + ('-lexville' if lexville else '-smallville') + '-full_' + str(ver) + '.zip'
	makezip(zipname, 'README.md', 'LICENSE', LEXVILLE_FOLDER if lexville else SMALLVILLE_FOLDER)
	return zipname
	

if __name__ == "__main__":
	if "CI" in os.environ:
		ver = int(os.environ["CI_COMMIT_REF_NAME"][4:])
	else:
		ver = int(sys.argv[1])
	print("   Build all patches from {} to {}".format(OLDEST_PATCH, ver))
	mklexville()
	build_full(ver, False)
	build_full(ver, True)
	for v in range(OLDEST_PATCH, ver):
		build_patch(v, ver, False)
		build_patch(v, ver, True)
	
		