
import os
import os.path
import shutil
import sys

def convert_one_file(source, destination, before = "NormalSchool", after = "ModSchool"):
	with open(source, 'rt', encoding='utf8') as input:
		with open(destination, 'wt', encoding='utf8') as output:
			output.write(input.read().replace(before, after))

def find_all_files(path):
	return (os.path.join(dp, f).split(os.path.sep, 1)[1] for dp, dn, filenames in os.walk(path) for f in filenames)

def process_folder(source, destination):
	# print("Copying folder")
	# shutil.copytree(source, destination)
	for f in find_all_files(source):
		if f.endswith('.ve.xml'):
			print("Converting " + f)
			convert_one_file(os.path.join(source, f), os.path.join(destination, f), source, destination)

if __name__ == "__main__":
	if len(sys.argv) > 2:
		process_folder(sys.argv[1], sys.argv[2])
	else:
		exit(1)
