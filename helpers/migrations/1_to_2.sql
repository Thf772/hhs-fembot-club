CREATE TABLE IF NOT EXISTS botclub_migrations (
	id INTEGER PRIMARY KEY,
	migration_level INTEGER NOT NULL
);

INSERT INTO botclub_migrations (id, migration_level)
VALUES (0, 2);