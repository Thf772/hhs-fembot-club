CREATE TABLE IF NOT EXISTS robot_club_person_status (
	person_id INTEGER PRIMARY KEY,
	robot_name VARCHAR(255) NOT NULL,
	robot_level INTEGER NOT NULL DEFAULT 0,
	update_day INTEGER DEFAULT -1,
	gender_at_conversion VARCHAR(64) NOT NULL DEFAULT 'Female',
	default_gender VARCHAR(64) NOT NULL DEFAULT 'Female',
	genitals_deployed BOOLEAN NOT NULL DEFAULT FALSE,
	force_gender VARCHAR(64) NOT NULL DEFAULT '',
	force_gender_cl INTEGER NOT NULL DEFAULT -1,
	force_outfit_level INTEGER NOT NULL DEFAULT -1,
	force_outfit_cl INTEGER NOT NULL DEFAULT -1,
	force_location VARCHAR(255) NOT NULL DEFAULT '',
	force_location_cl INTEGER NOT NULL DEFAULT -1,
	force_deploy_genitals INTEGER NOT NULL DEFAULT -1,
	force_genitals_cl INTEGER DEFAULT -1
);

CREATE TABLE IF NOT EXISTS robot_club_person_control (
	person_id INTEGER NOT NULL,
	controller_id INTEGER NOT NULL,
	controller_designation VARCHAR(255) NOT NULL DEFAULT 'Controller',
	control_level INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY (person_id, controller_id)
);

CREATE TABLE IF NOT EXISTS botclub_migrations (
	id INTEGER PRIMARY KEY,
	migration_level INTEGER NOT NULL
);

INSERT INTO botclub_migrations (id, migration_level)
VALUES (0, 2);