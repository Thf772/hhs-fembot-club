> ### TL;DR: Installing this from the ZIP archive:
> 
> **Do not use `Download zip` if you want to install for Lexville. Use `Download 'lexville'` only.**
>
>  - Step 1: Extract the contents of the archive into the Schools directory of your game installation
>  - Step 2: Say "Yes" whenever asked to overwrite
>  - Step 3: If not using Lexville, delete the ModSchool directory
>  - Step 4: Profit. Unlock the club by triggering a random encounter in the Schoolyard.


# Robotics/Robotification Club for HentHigh School + "Smallville"

Hello world, NESI here!

This is my first mod for the game [HentHigh School +][1]. It's a new club for your students to be in.

This club is about robots (duh) and will increase mostly Education and Intelligence (at least in the first levels).
The next levels will have the students turned into robots, with visible effect on the paperdolls.
Also contains a special student with a light and subtle reference to a famous movie about robots.

The club is hard to unlock and hard to upgrade by design, but it is compensated
by a high stat gain during club time events and additional bonuses
in later levels.

You can download it by clicking the "Download" button (small cloud with an arrow)
and picking `Download 'smallville'` or `Download 'lexville'` in the list.
Or you can download it from [the forum post][2].
Do not use `Dowload zip` or another of the archive formats,
they are just a copy of the Git repository.

> You can also download the in-development **and probably broken** version
by switching to the `master` branch or cloning the repository.

Because this mod contains a new club and a new NPC, you need a new game for it to work.
Unfortunately there's no workaround for that yet.

I hope you'll enjoy it!

Warmly yours,
NESI

## Current progress

This is the status on the Release branch, aka the downloads available on the forum.

 - Outfit variants for robotization levels: Done, partially tested
 - Bot club uniform: Done, partially tested
 - Protagonist: Done and tested (more ideas not started yet)
 - Level 0, Robotics Club: Done and fully tested
 - Level 1: Cyborgs Club: Done and fully tested, but I might add some events in the future
 - Level 2: Gynoids Club: In progress, available.
 - Level 3: Not started

## Overwritten base game files

This mod will be broken by any update or mod that replaces one of the following files:

 - `Events\PaperDollHandlers\PD_Student.ve.xml`
 - `Events\FunctionLibrary\PaperDoll\PDF_Student_Club.ve.xml`

## Building the Lexville version from the repo

> **You do not need to do this if you download from MEGA or from `Download 'lexville'`.**
> This is here for the sake of documentation.
> If you're reading this section, I assume you're familiar with command line and scripts.

The `ModSchool.overwrites` folder only contains a few of the events needed in the Lexville version.
I made a very basic Python script to convert those from Smallvile to Lexville.

You need to install Python 3, then run the following commands in the repo folder:

On Linux or WSL (recommended):

    mkdir ModSchool
    cp -Rf NormalSchool/* ModSchool
    python3 helpers/convert-to-lexville.py NormalSchool ModSchool
    cp -Rf ModSchool.overwrites/* ModSchool

On Windows/CMD

    mkdir ModSchool
    xcopy /i /s /e /y NormalSchool ModSchool
    python helpers\convert-to-lexville.py NormalSchool ModSchool
    xcopy /i /s /e /y ModSchool.overwrites ModSchool

On Windows/Powershell:

    New-Item -ItemType directory -Path ModSchool
    Copy-Item NormalSchool\* -Destination ModSchool -Recurse -Force
    Python helpers\convert-to-lexville.py NormalSchool ModSchool
    Copy-Item ModSchool.overwrites\* -Destination ModSchool -Recurse -Force


    [1]: https://www.henthighschool.com/hhsplus/
    [2]: https://www.henthighschool.com/hhs-mods/(mod)(hhs1-9-5)-robotics-club/